const fs = require("fs");
const path = require("path");
const filesDirPath = path.join(__dirname, 'files');

const extensions = ['.json', '.txt', '.html', '.js', '.log', '.yaml', '.xml'];

function createFile (req, res) {
  if (!req.body.content || !req.body.filename) {
    res.status(400).json({ message: `Please specify '${req.body.content ? 'filename' : 'content'}' parameter` });
  }

  const extension = path.extname(req.body.filename);
  if (!extension) {
    res.status(400).json({ "message": "No extension specified" });
  }

  if (!extensions.includes(extension)) {
    res.status(400).json({ "message": "Extension not supported" });
  }

  fs.writeFileSync(path.join(__dirname, 'files', req.body.filename), req.body.content);
  res.status(200).send({ "message": "File created successfully" });
}

function getFiles (req, res) {
  const findDir = fs.opendirSync(filesDirPath);

  if (!findDir) {
    res.status(400).json({ message: 'Client error' });
  }

  res.status(200).json({ message: 'Success', files: fs.readdirSync(filesDirPath) });
}

const getFile = (req, res) => {
  const filePath = path.join(filesDirPath, req.params.filename);

  if (!fs.existsSync(filePath)) {
    res.status(400).json({ message: `No file with '${req.params.filename}' filename found` });
  }

  res.status(200).json({
    message: 'Success',
    filename: req.params.filename,
    content: fs.readFileSync(filePath, 'utf8'),
    extension: path.extname(filePath).slice(1),
    uploadedDate: fs.statSync(filePath).birthtime
  });
}

const deleteFile = (req, res) => {
  const filePath = path.join(filesDirPath, req.params.filename);

  if (!fs.existsSync(filePath)) {
    res.status(400).json({ message: `No file with '${req.params.filename}' filename found` });
  }

  fs.unlinkSync(filePath);
  res.status(200).json({ message: `File '${req.params.filename}' has been successfully deleted` });
}

const editFile = (req, res) => {
  const filePath = path.join(filesDirPath, req.params.filename);

  if (!fs.existsSync(filePath)) {
    res.status(400).json({ message: `No file with '${req.params.filename}' filename found` });
  }

  fs.writeFileSync(filePath, req.body.content, { flag: 'w' });
  res.status(200).json({ message: 'File has been successfully edited' });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile
}
